/*
 * https://www.essentialmath.com/GDC2015/VanVerth_Jim_DrawingAntialiasedEllipse.pdf
 */

#include <stdlib.h>
#include <math.h>
#include <gtk/gtk.h>

static int fuzzy = 0;

typedef struct {
  double a, b, c, d;
} Mat;

static void
multiply (Mat *m1, Mat *m2, Mat *res)
{
        /* res = m1 * m2; */
        res->a = m1->a * m2->a + m1->b * m2->c;
        res->b = m1->a * m2->b + m1->b * m2->d;
        res->c = m1->c * m2->a + m1->d * m2->c;
        res->d = m1->c * m2->b + m1->d * m2->d;
}

static void
invert (Mat *m, Mat *res)
{
        double det;

        /* res = m^-1 */
        det = m->a * m->d - m->b * m->c;
        res->a = m->d / det;
        res->b = - m->b / det;
        res->c = - m->c / det;
        res->d = m->a / det;
}

static void
set_rotation (Mat *m, double angle)
{
        double s, c;

        s = sin (angle);
        c = cos (angle);

        m->a = c;
        m->b = -s;
        m->c = s;
        m->d = c;
}

static void
set_scale (Mat *m, double s1, double s2)
{
        m->a = s1;
        m->b = 0;
        m->c = 0;
        m->d = s2;
}

static void
apply (Mat *m, double x, double y, double *xres, double *yres)
{
        *xres = m->a * x + m->b * y;
        *yres = m->c * x + m->d * y;
}

static Mat t;
static Mat tinv;

static double
f (double x, double y)
{
        double x0, y0;

        apply (&t, x, y, &x0, &y0);

        return x0 * x0  + y0 * y0 - 1;
}

static double
dfdx (double x, double y)
{
  return 2 * x;
}

static double
dfdy (double x, double y)
{
  return 2 * y;
}

static void
delta_f (double x, double y, double *dx, double *dy)
{
  apply (&t, 2 * x, 2 * y, dx, dy);
}

static double
length (double x, double y)
{
        return sqrt (x * x + y * y);
}

static double
distance (double x, double y)
{
        double x0, y0, dx, dy;

        apply (&t, x, y, &x0, &y0);

        delta_f (x0, y0, &dx, &dy);

        return f (x, y) / length (dx, dy);
}

static double
coverage2 (double x, double y)
{
        if (f (x, y) < 0)
                return 1.0;
        else
                return 0.0;
}

static double
coverage (double x, double y)
{
        gdouble d;

        d = distance (x, y);

        if (-1 < d && d < 1) {
                return CLAMP (0.5 - d, 0.0, 1.0);
        }
        else
                return f(x, y) < 0 ? 1.0 : 0.0;
}


static gboolean
draw (GtkWidget *widget, cairo_t *cr)
{
        int width, height;
        int stride;
        int x, y;
        double x0, y0;
        cairo_surface_t *surface;
        guint32 *data, *p;
        guint red, green, blue;
        Mat scale;
        Mat rot;

        set_rotation (&rot, M_PI/3.0);
        set_scale (&scale, 60, 120);
        multiply (&rot, &scale, &tinv);
        invert (&tinv, &t);

        width = gtk_widget_get_allocated_width (widget);
        height = gtk_widget_get_allocated_height (widget);

        stride = cairo_format_stride_for_width (CAIRO_FORMAT_RGB24, width);
        data = g_malloc (height * stride);

        for (y = 0; y < height; y++) {
                y0 = y - height / 2;
                p = data + y * (stride / 4);
                for (x = 0; x < width; x++) {
                        x0 = x - width / 2;
                        if (fuzzy) {
                                red = green = blue = 255 * (1.0 - coverage (x0, y0));
                        }
                        else {
                                red = green = blue = 255 * (1.0 - coverage2 (x0, y0));
                        }

                        p[x] = (red << 16) | (green << 8) | blue;
                }
        }

        surface = cairo_image_surface_create_for_data ((guchar *)data, CAIRO_FORMAT_RGB24, width, height, stride);
        cairo_set_source_surface (cr, surface, 0, 0);
        cairo_paint (cr);
        cairo_surface_destroy (surface);
        g_free (data);

        return TRUE;
}

int main (int argc, char *argv[])
{
        GtkWidget *window;
        GtkWidget *da;

        gtk_init (NULL, NULL);

        if (argc > 1)
                fuzzy = atoi(argv[1]);
        window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
        gtk_window_set_default_size (GTK_WINDOW (window), 400, 400);
        da = gtk_drawing_area_new ();
        g_signal_connect (da, "draw", G_CALLBACK (draw), NULL);
        gtk_container_add (GTK_CONTAINER (window), da);

        gtk_widget_show_all (window);

        gtk_main ();

        return 0;
}
